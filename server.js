// process.env.NODE_ENV = process.env.NODE_ENV ? process.env.NODE_ENV : "local";
// require("dotenv").config({
//     path: `.env.${process.env.NODE_ENV}`
// });
const express=require("express");
const http = require('http')
const swaggerUi=require("swagger-ui-express");
const YAML=require("yamljs");
const services=require("./services/index");



const app=express();
app.use(express.json());
let server=require("http").createServer(app);

const io = require('socket.io')(server)
const socket=require('./v1/sockets/index')
const swaggerDocument = YAML.load("./swagger/collection.yml");
const router=require("./v1/routes/index");
app.use(express.json());
app.use("/api", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use("/v1/",router);
server.listen(7000,async()=>{
    require('./common/connection')
    socket(io);
    services.Swagger.createSwagger();
});
 
