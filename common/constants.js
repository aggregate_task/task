module.exports={
    MESSAGES:{
        POSTERROR:"error in creating details!",
        POSTSUCESS:"creation successful!!",
        EMAIL_ALREADY_EXISTS:"email already exists! Please try with new one!!",
        MATCH_UNSUCCESS:"data doesn't match!!",
        PROFILE_MATCHED:"Profile match sucessfully!!",
        PHONE_ALREADY_EXISTS:"this number already exists! Please try with new one!!",
        USER_NOT_EXIST:"user doesn't exist!!",
        FETCHING_SUCCESS:"fetching sucessful!",
        FETCHING_UNSUCCESS:"fetching unsuccessful!!",
        UPDATE_SUCCESS:"updating sucessful!!",
        UPDATE_UNSUCCESS:"updating unsucessful!!",
        DELETE_SUCCESS:"deletion successful!!",
        DELETE_UNSUCCESS:"deletion  unsucessful!!",
        BOOK_NOT_EXIST:"book doesn't exists!!or provide membership details",
        BORROWING_SUCCESS:"borrowing successful!!Please return within 15 days!!",
        CANT_RENEW:"you can't renew as the return date exceeded",
        RENEW_EXTENDED:"Renew extended for another 14 days!!"
    }
}