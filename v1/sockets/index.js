const CONSTANTS = require('../../common/constants');
const Models = require('../../models')
module.exports = (io) => {
    io.on('connection', (socket) => {
        console.log('a user connected');

        // a room between librarian and user 
        socket.on("join-room", async (data) => {
            try {
                console.log("hii");
                console.log(data);

                let userid = data.userid;
                let libraryid = data.libraryid;
                console.log(userid, libraryid)

                // to check whether id exists in db or not
                let checkId = await Models.SendMsg.findOne({ libraryid: libraryid }, { userid: userid })
                console.log("res", checkId);
                if (checkId === null) {
                    console.log("creating details")
                    let result = await Models.SendMsg.create({
                        userid: userid,
                        libraryid: libraryid,
                    })
                    socket.emit('connectroom', result)
                }
                else {

                    socket.join(checkId._id);
                    console.log(checkId._id, "room joined by 2 users")
                    console.log("user joined")
                    socket.emit("connectroom", "connected")
                }
            } catch (error) {
                socket.emit('connectroom', "error");
            }
        })

        // to send msg thorugh that room(msg and roomid)
        socket.on('sendmsg', async (data) => {
            console.log(data);
            let roomId = data._id;
            console.log(roomId)
            let result = await Models.SendMsg.findOne({ _id: roomId });
            console.log("result", result);
            if (result) {
                console.log(data.message);
                let existingdata = await Models.AllMsgs.create({
                    RoomId: data._id,
                    RoomMsg: data.message,
                    libraryid: data.libraryid,
                    userid: data.userid

                });
                console.log("result", existingdata);
                socket.to(roomId).emit('receive-msg', { msg: data.message, msg2: result });
            }
        })

        // leave-room
        socket.on('leave-room', async (data) => {
            try {
                console.log('hello')
                let roomId = data._id
                console.log(roomId);
                console.log(data);
                let result = await Models.SendMsg.findOne({ _id: roomId });
                console.log("result", result);
                if (result._id) {
                    socket.leave(roomId);
                    console.log('user has left the room', data.userid)
                    socket.to(roomId).emit('left-room', data.userid)
                }
            } catch (error) {
                socket.emit('left-room', "error")
            }
        })
        // Handle disconnection
        socket.on('disconnect', () => {
            console.log('User disconnected');
        });
    })
}