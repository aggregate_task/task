const express=require('express');
const router=express.Router();
const Controller=require('../controller');

// routes CRUD librarian
router.get('/getall',Controller.LibrarianController.getall);
router.post('/create',Controller.LibrarianController.postlibrarian);
router.post('/login',Controller.LibrarianController.postlogin);
router.post('/profile',Controller.LibrarianController.verifyToken,Controller.LibrarianController.verify)

router.get('/getinfo/:_id',Controller.LibrarianController.getinfo);
router.put('/updateinfo/:_id',Controller.LibrarianController.updateinfo);


// crud books
router.post('/addbooks/:_id',Controller.LibrarianController.addbooks);
router.put('/updatebooks/:librarianId',Controller.LibrarianController.updatebooks);


module.exports=router;