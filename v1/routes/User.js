const express=require('express');
const router=express.Router();
const Controller=require('../controller');

// routes
router.post('/getall',Controller.UserController.getall)
router.post('/create',Controller.UserController.postuser);
router.post('/login',Controller.UserController.postlogin);
router.post('/getaccessed',Controller.UserController.verifyToken,Controller.UserController.verify)
router.post('/checkout/:_id',Controller.UserController.checkout)
router.post('/renewbooks/:UserId',Controller.UserController.renewbooks)
router.post('/returnbooks/:UserId',Controller.UserController.returnbooks)
router.get('/aggregate/:userid',Controller.UserController.aggregate)
module.exports=router;
