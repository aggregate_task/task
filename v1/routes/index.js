const express=require('express');
const router=express.Router();
const LibrarianRoutes=require('./Librarian');
const UserRoutes=require('./User')

router.use('/librarian',LibrarianRoutes);
router.use('/user',UserRoutes);

module.exports=router;
