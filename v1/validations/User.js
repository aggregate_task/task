const joi=require('joi');


module.exports.postapi = joi.object({
    fname: joi.string().required(),
    lname:joi.string().required(),
    phone:joi.string().pattern(/^\d{10}$/).required(),
    email:joi.string().required(),
    password:joi.string().required()
});