const joi=require('joi');


module.exports.postapi = joi.object({
    fname: joi.string().required(),
    lname:joi.string().required(),
    email: joi.string().email().required().error(new Error("Enter valid email!!")),
    password: joi.string().required(),
    phone:joi.string().optional()
});