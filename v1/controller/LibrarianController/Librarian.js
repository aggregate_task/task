const mongoose = require('mongoose');
const mongodb=require('mongodb');
const Models = require('../../../models');
const jwt = require('jsonwebtoken');
const brcypt = require('bcrypt');
const CONSTANTS = require('../../../common/constants')
const Validation = require('../../validations');
const { v4: uuidv4 } = require('uuid');

const secretKey = "secretkey";


module.exports.getall = async (req, res) => {
    try {
        let page = Number(req.query.page);
        console.log(page);
        let limit = Number(req.query.limit);
        let skip = (page - 1) * limit;
        let search = req.query.fname;
        console.log(search);
        
        // taking an empty query object
        let query = {};
        if (search) {
            const regex=new RegExp('.*' + search + '.*', 'i');
            query.fname = { $regex:regex};
            console.log(query.fname);
        }
        // console.log(query);

        // Fetching data based on search query and pagination
        let data = await Models.Librarian.find(query)
            .sort({ updatedAt: -1 })
            .skip(skip)
            .limit(limit);
        // Counting the total number of documents matching the search query
        let length = await Models.Librarian.countDocuments(query);

        return res.json({
            msg:CONSTANTS.MESSAGES.FETCHING_SUCCESS,
            data:data,
            length:length
        });
    } catch (error) {
        return res.json({ 
            msg:CONSTANTS.MESSAGES.FETCHING_UNSUCCESS,
            error: error 
        });
    }
};


module.exports.postlibrarian = async (req, res) => {
    try {
        // validations
        await Validation.Librarian_Valid.postapi.validateAsync(req.body);
        let checkemail = await Models.Librarian.findOne({ email: req.body.email });
        console.log(checkemail);

        // validating whether email already exists
        if (checkemail) {
            return res.json({ message: CONSTANTS.MESSAGES.EMAIL_ALREADY_EXISTS })
        }
        else {
            let pwd = req.body.password;
            let hashpwd = await brcypt.hash(pwd, 10);
            console.log(hashpwd)
            let result = await Models.Librarian.create({
                fname: req.body.fname,
                lname: req.body.lname,
                email: req.body.email,
                password: hashpwd,
                phone: req.body.phone
            });
            return res.json({
                message: CONSTANTS.MESSAGES.POSTSUCESS,
                data: result
            })
        }
    } catch (error) {
        return res.json({
            message: CONSTANTS.MESSAGES.POSTERROR,
            error: error
        })
    }
}




module.exports.postlogin = async (req, res) => {
    let email = req.body.email;
    let pwd = req.body.password;
    let checkuser = await Models.Librarian.findOne({ email: email });
    console.log(checkuser);
    if (checkuser) {
        let matchpwd = await brcypt.compare(pwd, checkuser.password);
        console.log(matchpwd);
        let checkId = checkuser._id;
        if (matchpwd) {
            // random jti will be generated
            let jti = uuidv4();
            console.log(jti);
            let jtiUpdate = await Models.Librarian.updateOne({ _id: checkId }, { $set: { jti: jti } });
            console.log(jtiUpdate);
            // generating jwt token
            jwt.sign({ checkId: checkId, jti: jti }, secretKey, { expiresIn: '1h' }, (err, token) => {
                if (err) {
                    console.log(err);
                }
                else {
                    console.log(token);
                    return res.json({ token: token });
                }
            })
        }
    }
};


// validating the jwt token
module.exports.verify = async (req, res, next) => {
    try {
        jwt.verify(req.token, secretKey, async (err, authData) => {
            if (err) {
                res.send({ result: "invalid token" });
            } else {
                console.log(authData);
                // verifying the jti to exist in db
                const jtiId = authData.jti;
                console.log(jtiId);

                // checking if jti exists in db
                let data = await Models.Librarian.findOne({ _id: authData.checkId }, { jti: jtiId });
                console.log(data);
                if (data) {
                    return res.json({
                        message: CONSTANTS.MESSAGES.PROFILE_MATCHED,
                        data: data
                    })
                }
                else {
                    return res.json({
                        message: CONSTANTS.MESSAGES.MATCH_UNSUCCESS
                    });
                }
            }
        });
    }
    catch (error) {
        return res.json({ error: error })
    }
};

// verifytoken
module.exports.verifyToken = async (req, res, next) => {
    const bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== "undefined") {
        const bearer = bearerHeader.split(" ");
        const token = bearer[1];
        req.token = token;
        next();
    }
};


// view the profile
module.exports.getinfo = async (req, res) => {
    try {
        let details = await Models.Librarian.findById(req.params);
        console.log(details);
        return res.json({
            message: CONSTANTS.MESSAGES.FETCHING_SUCCESS,
            data: details
        })
    } catch (error) {
        return res.json({
            message: CONSTANTS.MESSAGES.FETCHING_UNSUCCESS,
            error: error
        })
    }
}

// updateinfo by id
module.exports.updateinfo = async (req, res) => {
    try {
        let existingUser = await Models.Librarian.findById(req.params);
        console.log(existingUser)
        if (existingUser._id) {
            let result = await Models.Librarian.updateOne({ _id: req.params }, { $set: req.body });
            console.log(result)
            return res.json({
                message: CONSTANTS.MESSAGES.UPDATE_SUCCESS,
                data: result
            })
        }
    } catch (error) {
        return res.json({
            message:CONSTANTS.MESSAGES.UPDATE_UNSUCCESS,
            error: error
        })
    }
}




// CRUD BOOKS
module.exports.addbooks=async(req,res)=>{
    try{
    let libraryId=req.params;
    console.log(libraryId);
    // check if exists in db
    let checkuser=await Models.Librarian.findById(req.params);
    console.log(checkuser)
    if (checkuser._id){
        let data=await Models.Catalogue.create({
            book_title:req.body.book_title,
            book_author:req.body.book_author,
            librarianId:req.params
        });
        console.log(data);
        return res.json({
        message:CONSTANTS.MESSAGES.POSTSUCESS,
        data:data
    })
    }
    }catch(error){
        return res.json({
            message:CONSTANTS.MESSAGES.POSTERROR,
            error:error
        })
    }
}


module.exports.updatebooks = async (req, res) => {
    try {
        let librarianId=req.params.librarianId;
        console.log(librarianId);
        let newlibId=new mongodb.ObjectId(librarianId);
        console.log(newlibId);

        let existingUser = await Models.Catalogue.findOne({librarianId:newlibId});
        console.log("e",existingUser)
        if (existingUser.librarianId) {
            let result = await Models.Catalogue.updateOne({ librarianId: existingUser.librarianId }, { $set: req.body });
            console.log(result)
            return res.json({
                message: CONSTANTS.MESSAGES.UPDATE_SUCCESS,
                data: result
            })
        }
    } catch (error) {
        return res.json({
            message:CONSTANTS.MESSAGES.UPDATE_UNSUCCESS,
            error: error
        })
    }
}


module.exports.deletebooks=async(req,res)=>{
    try {
        let librarianId=req.params.librarianId;
        console.log(librarianId);
        let newlibId=new mongodb.ObjectId(librarianId);
        console.log(newlibId);

        let existingUser = await Models.Catalogue.findOne({librarianId:newlibId});
        console.log("e",existingUser)
        if (existingUser.librarianId) {
            let result = await Models.Catalogue.deleteOne({ librarianId: existingUser.librarianId })
            return res.json({
                message: CONSTANTS.MESSAGES.DELETE_SUCCESS,
                data: result
            })
        }
    } catch (error) {
        return res.json({
            message:CONSTANTS.MESSAGES.DELETE_UNSUCCESS,
            error: error
        })
    }
}