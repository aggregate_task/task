const mongoose = require('mongoose');
const mongodb = require('mongodb');
const Models = require('../../../models');
const jwt = require('jsonwebtoken');
const brcypt = require('bcrypt');
const CONSTANTS = require('../../../common/constants')
const Validation = require('../../validations');
const rn = require('random-number')
const { v4: uuidv4 } = require('uuid')
const date = require('date-and-time')
const secretKey = "secretkey";



module.exports.getall = async (req, res) => {
    try {
        let page = Number(req.query.page);
        console.log(page);
        let limit = Number(req.query.limit);
        let skip = (page - 1) * limit;
        let search = req.query.fname;
        console.log(search);
        
        // taking an empty query object
        let query = {};
        if (search) {
            const regex=new RegExp('.*' + search + '.*', 'i');
            query.fname = { $regex:regex};
            console.log(query.fname);
        }
        // console.log(query);

        // Fetching data based on search query and pagination
        let data = await Models.User.find(query)
            .sort({ updatedAt: -1 })
            .skip(skip)
            .limit(limit);
        // Counting the total number of documents matching the search query
        let length = await Models.User.countDocuments(query);

        return res.json({
            msg:CONSTANTS.MESSAGES.FETCHING_SUCCESS,
            data:data,
            length:length
        });
    } catch (error) {
        return res.json({ 
            msg:CONSTANTS.MESSAGES.FETCHING_UNSUCCESS,
            error: error 
        });
    }
};


module.exports.postuser = async (req, res) => {
    try {
        // validations
        await Validation.User_Valid.postapi.validateAsync(req.body);
        let checkphone = await Models.User.findOne({ phone: req.body.phone });
        console.log(checkphone);

        let membership_id = uuidv4();
        console.log(membership_id);

        // validating whether contact already exists
        if ( checkphone.phone) {
            return res.json({ message: CONSTANTS.MESSAGES.PHONE_ALREADY_EXISTS })
        }
        else {
            let pwd = req.body.password;
            let hashpwd = await brcypt.hash(pwd, 10);
            let result = await Models.User.create({
                fname: req.body.fname,
                lname: req.body.lname,
                phone: req.body.phone,
                email:req.body.email,
                password:hashpwd,
                membership_id: membership_id,
                
            });
            return res.json({
                message: CONSTANTS.MESSAGES.POSTSUCESS,
                data: result
            })
        }
    } catch (error) {
        return res.json({
            message: CONSTANTS.MESSAGES.POSTERROR,
            error: error
        })
    }
}




module.exports.postlogin = async (req, res) => {

    try {
        let email = req.body.email;
    let pwd = req.body.password;
        let checkuser = await Models.User.findOne({email:req.body.email});
        console.log(checkuser);
        if (checkuser) {
            let matchpwd = await brcypt.compare(pwd, checkuser.password);
            console.log(matchpwd);
            let checkId = checkuser._id;
            if (matchpwd) {
            // random jti will be generated
            let jti = uuidv4();
            console.log(jti);
            let jtiUpdate = await Models.User.updateOne({ _id: checkId }, { $set: { jti: jti } });
            console.log(jtiUpdate);
            // generating jwt token
            jwt.sign({ checkId: checkId, jti: jti }, secretKey, { expiresIn: '1h' }, (err, token) => {
                if (err) {
                    console.log(err);
                }
                else {
                    console.log(token);
                    return res.json({ token: token });
                }
            })
        }
    }
    }
    catch (error) {
        return res.json({
            msg: CONSTANTS.MESSAGES.USER_NOT_EXIST,
            error: error
        })
    }

};



// validating the jwt token
module.exports.verify = async (req, res, next) => {
    try {
        jwt.verify(req.token, secretKey, async (err, authData) => {
            if (err) {
                res.send({ result: "invalid token" });
            } else {
                console.log(authData);
                // verifying the jti to exist in db
                const jtiId = authData.jti;
                console.log(jtiId);

                // checking if jti exists in db
                let data = await Models.User.findOne({ _id: authData.checkId }, { jti: authData.jti })
                console.log(data);
                if (data) {
                    return res.json({
                        message: CONSTANTS.MESSAGES.PROFILE_MATCHED,
                        data: data
                    })

                }
                else {
                    return res.json({
                        message: CONSTANTS.MESSAGES.MATCH_UNSUCCESS
                    });
                }
            }
        })
    }
    catch (error) {
        return res.json({ error: error })
    }
};

// verifytoken
module.exports.verifyToken = async (req, res, next) => {
    const bearerHeader = req.headers["authorization"];
    if (typeof bearerHeader !== "undefined") {
        const bearer = bearerHeader.split(" ");
        const token = bearer[1];
        req.token = token;
        next();
    }
};



// PROFILE MGMT
module.exports.getinfo = async (req, res) => {
    try {
        let details = await Models.Librarian.findById(req.params);
        console.log(details);
        return res.json({
            message: CONSTANTS.MESSAGES.FETCHING_SUCCESS,
            data: details
        })
    } catch (error) {
        return res.json({
            message: CONSTANTS.MESSAGES.FETCHING_UNSUCCESS,
            error: error
        })
    }
}


//borrowing and returns

// borrowing book
module.exports.checkout = async (req, res) => {
    try {
        // check whether user exists in db
        let checkuser = await Models.User.findById(req.params);
        console.log(checkuser)
        if (checkuser._id) {
            let book_title = req.body.book_title;
            let book_author = req.body.book_author;
            let membership_id = req.body.membership_id;
            console.log("m", membership_id)
            let borrowbymemid = await Models.User.findOne({ membership_id: membership_id });
            console.log("b", borrowbymemid)
            let borrowbydetails = await Models.Catalogue.findOne({ book_title: book_title, book_author: book_author });
            console.log("d", borrowbydetails)
            console.log(Date());
            // user can borrow either by membership_id or book details
            if (membership_id === "" && (book_title === "") && (book_author !== "")) {
                console.log("please enter details!!");
            }
            else {
                if (borrowbymemid.membership_id || borrowbydetails.book_title) {
                    let creation = await Models.BorrowBooks.create({
                        book_title: book_title,
                        book_author: book_author,
                        UserId: req.params,
                        borrowing: Date(),
                        // returns:date.addDays(now,14)
                    })
                    return res.json({
                        message: CONSTANTS.MESSAGES.BORROWING_SUCCESS,
                        data: creation
                    })
                }
            }
        }



    } catch (error) {
        return res.json({
            message: CONSTANTS.MESSAGES.BOOK_NOT_EXIST,
            error: error
        })
    }
}

// returns
module.exports.returnbooks = async (req, res) => {
    try {
        let userid = req.params.UserId;
        let newid = new mongodb.ObjectId(userid);
        let book_title = req.body.book_title;
        let book_author = req.body.book_author;

        // find the email
        let useremail=await Models.User.findOne({_id:newid});
        console.log(useremail.email);

        let checkuser = await Models.BorrowBooks.findOne({
             $and: [{ UserId: newid }, 
                { book_title: book_title }, 
                { book_author: book_author }] });
        console.log(checkuser);
        if (checkuser.UserId) {
            let retuned=await Models.BorrowBooks.updateOne({_id:checkuser._id},{
                $set:{returns:Date.now()}
            })
            return res.json({
                data:retuned
            })
        }
    } catch (error) {
        return res.json({
            error:error
        })
    }
}


// renew
module.exports.renewbooks = async (req, res) => {
    try {
        // check return date 
        console.log('hi')
        let borrowDate = await Models.BorrowBooks.findById(req.params);
        console.log(borrowDate);
        if (Date.now() > borrowDate.returns) {
            return res.json({ message: CONSTANTS.MESSAGES.CANT_RENEW })
        }
        else {
            let data = await Models.BorrowBooks.updateOne({ _id: borrowDate._id }, {
                $set: {
                    renew: Date.now()
                }
            })
            return res.json({
                message: CONSTANTS.MESSAGES.RENEW_EXTENDED,
                data: data
            })
        }
    } catch (error) {
        return res.json({
            error: error
        })
    }
}



// get userdetails from borrowbooks
module.exports.aggregate=async(req,res)=>{
    // console.log(req.params);
    let userId=req.params.userId;
    console.log(userId);
    const newid=new mongodb.ObjectId(userId);
   
    let result=await Models.BorrowBooks.aggregate(
        [
            
            {$match: {userId:newid}},
            { "$limit" : 1 }, 
            { "$skip" : 0 },
            {$project:{
                createdAt:false, updatedAt:false
            }},
        {$lookup:{from:'users',localField:'UserId', foreignField:'UserId',as:'data'}},
        {$unwind:"$data"},
        
        {
            $project: {
                "data.createdAt" : 0,
                "data.updatedAt":0
            }
        }
    ])
    return res.json({
        message:'success!!',
        data:result
    })
}



