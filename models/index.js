module.exports={
    Librarian:require('./Librarian_Sch_Mod'),
    User:require('./User_Sch_Mod'),
    Catalogue:require('./Catalogue_Sch_Mod'),
    BorrowBooks:require('./BorrowBooks_Sch_Mod'),
    SendMsg:require('./SendMsg_Sch_Mod'),
    AllMsgs:require('./AllMsgs_Sch_Mod')
}