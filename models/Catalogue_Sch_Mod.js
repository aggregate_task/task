
const mongoose=require('mongoose');
const CatalogueSch=new mongoose.Schema({
    librarianId:{
        type:mongoose.Types.ObjectId,
        ref:'librarian'
    },
    book_title:{
        type: String,
        default:"",
        index:true
    },
    book_author:{
        type:String,
        default:"",
        index:true
    },
    // jti:{
    //     type:String,
    //     required:true
    // }
},
{timestamps:
    true
},
);


const Catalogue=mongoose.model('books',CatalogueSch);
module.exports=Catalogue;