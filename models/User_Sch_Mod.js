
const mongoose=require('mongoose');
const UserSch=new mongoose.Schema({
    fname:{
        type: String,
        default:"",
        index:true
    },
    lname:{
        type: String,
        default:"",
        index:true
    },
    email:{
        type:String,
        required:true,
        index:true
    },
    password:{
        type:String,
        required:true,
        index:true
    },
    phone:{
        type: String,
        default:"",
        index:true
    },
    membership_id:{
        type:String,
        default:"",
        index:true
    },
    libraryId:{
        type:mongoose.Types.ObjectId,
        ref:'libraran'
    },
    jti:{
        type:String,
        required:true
    }

},
{timestamps:
    true
},
);


const Users=mongoose.model('users',UserSch);
module.exports=Users;