const mongoose=require('mongoose');
const BorrowBooksSch=new mongoose.Schema({
    UserId:{
        type:mongoose.Types.ObjectId,
        ref:'users'
    },
    book_title:{
        type: String,
        default:"",
        index:true
    },
    book_author:{
        type:String,
        default:"",
        index:true
    },
    borrowing:{
        type:Date,
        index:true
    },
    returns:{
        type:Date,
        index:true
    },
    renew:{
        type:Date,
        index:true
    }

    // jti:{
    //     type:String,
    //     required:true
    // }
},
{timestamps:
    true
},
);


const BorrowBooks=mongoose.model('booksinfo',BorrowBooksSch);
module.exports=BorrowBooks;