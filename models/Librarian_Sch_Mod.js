
const mongoose=require('mongoose');
const LibrarianSch=new mongoose.Schema({
    fname:{
        type: String,
        default:"",
        index:true
    },
    lname:{
        type: String,
        default:"",
        index:true
    },
    email:{
        type: String,
        tolowercase:true,
        default:"",
        index:true
    },
    password:{
        type: String,
        default:"",
        index:true
    },
    phone:{
        type: String,
        default:"",
        index:true
    },
    jti:{
        type:String,
        required:true
    }
},
{timestamps:
    true
},
    {
        toJSON:{
            transform(doc,ret){
                delete ret.password;
            }
        }
    }
);


const Librarian=mongoose.model('librarian',LibrarianSch);
module.exports=Librarian;